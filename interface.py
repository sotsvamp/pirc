import curses

KEY_ENTER = 13
KEY_BACKSPACE = 127
KEY_NOTHING = 195
KEY_ARING_LC = 165
KEY_ARING_UC = 133
KEY_ADOTS_LC = 164
KEY_ADOTS_UC = 132
KEY_ODOTS_LC = 182
KEY_ODOTS_UC = 150

CHAR_ARING_LC = 229
CHAR_ARING_UC = 194
CHAR_ADOTS_LC = 228
CHAR_ADOTS_UC = 196
CHAR_ODOTS_LC = 246
CHAR_ODOTS_UC = 214

BLUE = 1
CYAN = 2
GREEN = 3
MAGENTA = 4
RED = 5
WHITE = 6
YELLOW = 7

colors = {
    BLUE:       curses.COLOR_BLUE,
    CYAN:       curses.COLOR_CYAN,
    GREEN:      curses.COLOR_GREEN,
    MAGENTA:    curses.COLOR_MAGENTA,
    RED:        curses.COLOR_RED,
    WHITE:      curses.COLOR_WHITE,
    YELLOW:     curses.COLOR_YELLOW}

class TextUI:
    def __init__(self, stdscr):

        stdscr.clear()
        curses.nonl()

        self.server_window = curses.newwin(curses.LINES-4, curses.COLS-1, 0, 0)
        self.server_window.border()
        self.server_window.nodelay(1)

        self.input_window = curses.newwin(3, curses.COLS-1, curses.LINES-4, 0)
        self.input_window.border()
        self.input_window.nodelay(1)
        self.input_window.move(1, 1)

        self.server_window.refresh()
        self.input_window.refresh()

        self.server_line = 1
        self.input_pos = 1
        self.current_color = WHITE

        self.user_input = ""

        for number, val in colors.items():
            curses.init_pair(number, colors[number], curses.COLOR_BLACK)

    def set_text_color(self, color):
        self.current_color = color

    def handle_enter_key(self, key):
        # Enter
        msg_from_user = self.user_input
        self.user_input = ""
        self.input_pos = 1
        self.input_window.clear()
        self.input_window.border()
        self.input_window.move(1, 1);
        self.input_window.refresh()
        return msg_from_user
    def handle_backspace_key(self, key):
        # Backspace
        if len(self.user_input) == 0:
            return
        self.user_input = self.user_input[:-1]
        self.input_pos -= 1
        self.input_window.move(1, self.input_pos);
        self.input_window.addstr(" ")
        self.input_window.move(1, self.input_pos);
        self.input_window.refresh()
        return None
    def handle_normal(self, key):
        key2swedish_char = {KEY_ARING_LC: CHAR_ARING_LC,
                            KEY_ARING_UC: CHAR_ARING_UC,
                            KEY_ADOTS_LC: CHAR_ADOTS_LC,
                            KEY_ADOTS_UC: CHAR_ADOTS_UC,
                            KEY_ODOTS_LC: CHAR_ODOTS_LC,
                            KEY_ODOTS_UC: CHAR_ODOTS_UC}
        if key in key2swedish_char:
            actual_char = chr(key2swedish_char[key])
        else:
            actual_char = chr(key)
        self.user_input += actual_char
        self.input_window.move(1, self.input_pos);
        self.input_pos += 1
        self.input_window.addstr(actual_char)
        self.input_window.refresh()
        return None
    def no_func(self, key):
        return None

    def input(self):
        keyboard_mapping = { -1            : self.no_func,
                             KEY_NOTHING   : self.no_func,
                             KEY_ENTER     : self.handle_enter_key,
                             KEY_BACKSPACE : self.handle_backspace_key}

        key = self.input_window.getch()

        if key in keyboard_mapping:
            return keyboard_mapping[key](key)
        else:
            return self.handle_normal(key)

    def output(self, msg):

        while msg:
            if self.server_line == (curses.LINES - 6):
                self.server_window.move(1,1)
                self.server_window.deleteln()
                self.server_window.move(self.server_line, 1)
                self.server_window.addstr(" " * (curses.COLS - 2))
                self.server_window.border()
            else:
                self.server_line += 1

            if len(msg) > curses.COLS - 2:
                part = msg[:curses.COLS - 3]
                line = part[:part.rfind(' ')]
                msg = msg[len(line):]
            else:
                line = msg
                msg = None

            self.server_window.move(self.server_line, 1)
            self.server_window.addstr(line, curses.color_pair(self.current_color))

            self.input_window.move(1, self.input_pos)
            self.server_window.refresh()
            self.input_window.refresh()
